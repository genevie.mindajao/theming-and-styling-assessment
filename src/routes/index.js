import Vue from 'vue'
import Router from 'vue-router'
import auth from './auth'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    auth,
    {
      path: '/',
      name: 'home',
      redirect: { name: 'inbox' },
      component: () => import(/* webpackChunkName: "home" */ '@/views/Home'),
      children: [
        {
          path: 'inbox',
          name: 'inbox',
          component: () =>
            import(/* webpackChunkName: "inbox" */ '@/views/Home/Inbox')
        },
        {
          path: 'users',
          name: 'users',
          component: () =>
            import(/* webpackChunkName: "users" */ '@/views/Home/Users')
        },
        {
          path: 'users/:id',
          name: 'user.details',
          component: () =>
            import(
              /* webpackChunkName: "users" */ '@/views/Home/Users/UserDetails'
            )
        },
        {
          path: 'content',
          name: 'content',
          component: () =>
            import(/* webpackChunkName: "content" */ '@/views/Home/Content')
        },
        {
          path: 'notification',
          name: 'notification',
          component: () =>
            import(
              /* webpackChunkName: "push-notification" */ '@/views/Home/Notification'
            )
        },
        {
          path: 'settings',
          name: 'settings',
          component: () =>
            import(/* webpackChunkName: "settings" */ '@/views/Home/Settings')
        },
        {
          path: 'reports',
          name: 'reports',
          component: () =>
            import(/* webpackChunkName: "reports" */ '@/views/Home/Reports')
        },
        {
          path: 'theme',
          name: 'theme',
          component: () =>
            import(/* webpackChunkName: "theme" */ '@/views/Home/Theme')
        },
        {
          path: 'assessment',
          name: 'assessment',
          component: () =>
            import(
              /* webpackChunkName: "assessment" */ '@/views/Home/Assessment'
            )
        }
      ]
    }
  ]
})
