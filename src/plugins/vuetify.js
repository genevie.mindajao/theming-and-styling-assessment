import Vue from 'vue'
import Vuetify from 'vuetify/lib'

Vue.use(Vuetify)

export default new Vuetify({
  icons: {
    iconfont: 'mdiSvg'
  },
  theme: {
    options: {
      customProperties: true
    },
    themes: {
      light: {
        primary: '#1062FF',
        secondary: '#5A489B',
        accent: '#8c9eff',
        error: '#FA4856',
        grey: {
          base: '#9E9E9E',
          lighten3: '#f1f3f4'
        }
      }
    }
  }
})
